# Claviers

Ressources autour des claviers mécaniques vraiment ergonomiques (donc fabriqués maison !)

# Liens

## Informations

### Français

+ https://claviers-mecaniques.fr/
+ https://commentgeek.com/comment-construire-clavier-personnalise/
+ https://lucidar.me/fr/keyboards/where-can-we-buy-french-keycaps/

### Étrangers

+ https://wiki.nikitavoloboev.xyz/keyboards
+ https://asylum.madhouse-project.org/blog/2016/10/15/multi-purpose-keys/
+ https://vlukash.com/2019/01/14/corne-crkbd-keyboard-build/
+ https://news.ycombinator.com/item?id=21798332
+ https://github.com/diimdeep/awesome-split-keyboards
+ https://keebfol.io/
+ https://thomasbaart.nl/ (la personne qui fait tourner la boutique [SplitKB](https://splitkb.com))
+ https://www.reddit.com/r/ErgoMechKeyboards/comments/fe52c9/kyria_interest_check_feature_survey/
+ https://www.reddit.com/r/ErgoMechKeyboards/
+ https://www.reddit.com/r/MechanicalKeyboards/comments/fhem68/dressed_up_imk_corne/
+ https://www.reddit.com/r/ErgoMechKeyboards/comments/e5app0/iris_vs_kyria_w_tenting/
+ https://mechlounge.com/interesting-ortho-ergo-keyboards/
+ https://mechbox.co.uk/
+ https://github.com/josefadamcik/SofleKeyboard
+ https://github.com/atulloh/oddball

## Boutiques en ligne

### France

+ https://yunk.fr

### Europe

+ https://splitkb.com/
+ https://mechboards.co.uk/
+ https://keycapsss.com/
+ https://candykeys.com/
+ https://www.wasdkeyboards.com/ (capuchons personnalisés)
+ https://mykeyboard.eu/
+ https://www.gotronic.fr/art-module-mini-trackball-pim447-31602.htm#complte_desc
+ https://www.gotronic.fr/art-capteur-de-flux-optique-pmw3901-pim453-31601.htm
+ https://splitkb.com/collections/keyboard-parts/products/trackball-breakout
